#pragma strict

private var PLAYER:GameObject;
private var BARCO:GameObject;

//altura a la que los pies tocan el piso
private var FLOOR_HEIGHT: float;

private var SPEED_Y = 0.0;
var SPEED_X = 2.0;

//Lista de enemigos a los que esta tocando
private var touchingList = new ArrayList();

//potencia del salto de entrada en la creacion del enemigo
public var initJump:float = 8;

private var attTIMER:float;
private var attDistance:float;
public var attDamage:int = 10;
public var attCooldown:float = 2;

//define si puede pasar a traves de otros enemigos
public var canGoThrough : boolean = false;

//puntaje que da al ser eliminado
public var scoreValue = 1;
//vida de regalo que entrega al pirata cuando muere
public var healthGiven :float = 0;

public var popUp : GameObject;

/*	La vida de los enemigos se representa por un arreglo, donde cada elemento representa un punto de vida
	y al mismo tiempo el tipo de golpe requerido para poder hacerle daño. El enemigo empieza en el primer
	elemento de este arreglo (0), por cada daño efectivo, avanza al siguiente elemento. Cuando no quedan
	mas, el enemigo muere.
	Elementos del arreglo:
	1,2,3: Indican que se necesita un golpe golpe ese tipo para que se haga daño
	-1,-2,-3: Cualquier golpe de tipo distinto a ese numero hace daño
	0: cualquier tipo decimal golpe hace daño */
public var LIFE:int[] = [0]; //valor por defecto
//es el indice de LIFE, cuenta cuando daño ha recibido el weon
private var damage:int = 0;

//Estados comportamiento enemigo
//IDLE: Estado default, hace nada
//APPROACHING: El enemigo se acerca a atacar al jugador, animado
//ATTACK: El enemigo ejecuta el ataque, animado
//HURT: El enemigo recibe daño, probablmente sería empujado hacia atrás, animado
//WAITING: El enemigo esta listo para atacar, espera su cooldown
//DYING: El enemigo ha muerto por lo que ocurre una animación de derrota.
private var STATE_IDLE:byte 		=	0;
private var STATE_APPROACHING:byte 	=	1;
private var	STATE_ATTACK:byte		=	2;
private var STATE_HURT:byte			=	3;
private var STATE_WAITING:byte		=	4;
private var STATE_DYING	:byte		=	5;
private var STATE_FALLING:byte		=	6;

var m_State  :  byte;



function Start () {
	//obtener objetos relevantes
	PLAYER =  GameObject.FindWithTag ("Player");
	attDistance  = transform.collider.bounds.extents.x + PLAYER.transform.collider.bounds.extents.x;
	BARCO =  GameObject.FindWithTag ("Barco");
	transform.parent = BARCO.transform;
	
	//altura a la que los pies tocan el piso
	FLOOR_HEIGHT = transform.collider.bounds.extents.y; // ...para considerar altura del barco + BARCO.transform.renderer.bounds.extents.y
	
	//definir posiciones
	transform.localPosition = Vector3(0.8*BARCO.transform.collider.bounds.extents.x,0,-.5);
	//random para izquierda o derecha
	if ( Random.Range(0,2) == 1)
		transform.localPosition.x = -transform.localPosition.x;
	
	//entrar al barco de un salto
	SPEED_Y = initJump;
	m_State = STATE_FALLING;
}

function Update () {
	if ( transform.localPosition.y > FLOOR_HEIGHT )
		m_State= STATE_FALLING;
	switch(m_State)
	{
	case STATE_IDLE:
		break;
		
	case STATE_APPROACHING:
		s_Approaching ();
		break;
		
	case STATE_ATTACK:
		s_Attack();
		break;
		
	case STATE_HURT:
		s_Hurt();
		break;
		
	case STATE_WAITING:
		if ( Mathf.Abs( transform.localPosition.x - PLAYER.transform.localPosition.x ) > attDistance )
			m_State = STATE_APPROACHING;
		else
			s_Waiting();
		break;
		
	case STATE_DYING:
		s_Dying();
		break;
		
	case STATE_FALLING:
		s_Falling();
		break;
		
	default:
		Debug.Log("ERROR: Estado no válido en enemigo. EnemyMove.js");
		break;
	}
	
}

function s_Falling () {
	SPEED_Y += -Physics.gravity.magnitude * Time.deltaTime;
	transform.localPosition.y += SPEED_Y * Time.deltaTime;
	
	if ( transform.localPosition.y < FLOOR_HEIGHT && SPEED_Y < 0) {
		transform.localPosition.y = FLOOR_HEIGHT;
		transform.localRotation.z = 0;
		m_State = STATE_APPROACHING;
		SPEED_Y = 0;
	}
}

function s_Approaching () {
	var canImove = true;
	for (var i = 0; i < touchingList.Count; i++){
		var enem = touchingList[i] as GameObject;
		//si esto es positivo, el otro enemigo esta en la misma direccion que speed
		if ( enem != null && (enem.transform.localPosition.x - transform.localPosition.x) * SPEED_X > 0){
			canImove = false;
		}
	}
	
	//moverse
	if (canImove)
		transform.localPosition.x += SPEED_X * Time.deltaTime;
		
	//revisar si esta en la direccion adecuada, si no, cambiar de direccion
	if (PLAYER.transform.localPosition.x < transform.localPosition.x && SPEED_X > 0) {
		SPEED_X = -SPEED_X;
	}
	else if (PLAYER.transform.localPosition.x > transform.localPosition.x && SPEED_X < 0) {
		SPEED_X = -SPEED_X;
	}
	//si esta en posicion de ataque
	if ( Mathf.Abs( transform.localPosition.x - PLAYER.transform.localPosition.x ) <= attDistance ) {
		attTIMER = 0;
		m_State = STATE_WAITING;
	}
}

function s_Waiting() {
	if (attTIMER >= attCooldown ) {
		attTIMER = 0;
		m_State = STATE_ATTACK;
	}
	else
		attTIMER += Time.deltaTime;
}

function s_Attack() {
	//Aqui tiene que esperar la animacion y atacar en el momento que corresponde
	PLAYER.gameObject.GetComponent(ParanoidHealth).AddHealth(-attDamage);
	m_State = STATE_WAITING;
}

function s_Hurt () {
	m_State = STATE_APPROACHING; //por mientras
}

function s_Dying () {
	//caer hasta morir
	SPEED_Y += -Physics.gravity.magnitude * Time.deltaTime;
	transform.localPosition.y += SPEED_Y * Time.deltaTime;
	transform.Rotate(0,0, 360*Time.deltaTime); //rotacion, just for fun
}

function DieAndDestroy() {
	//avisarle al barco que murio un enemigo
	BARCO.GetComponent(EnemyGenerator).EnemyKilled();
	//morir
	Destroy( gameObject );
}

function hit(type : int) {
	//solo ser golpeado si esta vivo
	if (m_State != STATE_DYING && m_State != STATE_FALLING ) {
		m_State = STATE_HURT;
		//si es positivo, tiene que ser el ataque particular que se pide
		if ( LIFE[damage] > 0 && LIFE[damage] == type)
			damage += 1;
		//si es 0 o negativo, cualquier ataque menos el que se indica sirve
		else if ( LIFE[damage] <= 0 && LIFE[damage] != -type)
			damage += 1;
		//este es el caso de que el tipo de ataque no es compatible, entonces se bloquea
		else {
			var pos = Vector3( Mathf.Sign(-SPEED_X)*0.2+0.44, 0.6, 0 );
			var obj = Instantiate (popUp, pos, Quaternion.identity );
			obj.GetComponent(popText).SetText("BLOCKED",.5,Color.red );
		}
		//murio?
		if (damage >= LIFE.Length) {
			m_State = STATE_DYING;
			//dar puntaje
			GameObject.FindWithTag("scoreText").GetComponent(ScoreText).AddScore(scoreValue);
			PLAYER.gameObject.GetComponent(ParanoidHealth).AddHealth(healthGiven);
		}
	}
}

//Funciones lista de enemigos tocando

function OnTriggerEnter(other : Collider) {
	if (!canGoThrough){
		if (other.gameObject.CompareTag("Enemy"))
			touchingList.Add(other.gameObject);
	}
}

function OnTriggerExit(other : Collider) {
	if (!canGoThrough){
		for (var i = 0; i < touchingList.Count; i++){
			if ( other.gameObject == touchingList[i] as GameObject)
				touchingList.RemoveAt(i);
		}
	}
}