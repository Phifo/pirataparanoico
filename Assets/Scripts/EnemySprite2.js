#pragma strict

private var timeCounter: float;
public  var loopTime: float = 1.5;
//marcar si tiene animacion de ataque
public var hasAttack : boolean = false;

private var	STATE_ATTACK:byte =	2;
private var STATE_WAITING:byte = 4;

private var PLAYER:GameObject;
private var PARENT:GameObject;

function Start () {
	timeCounter = 0;
	PLAYER =  GameObject.FindWithTag ("Player");
	PARENT = transform.parent.gameObject;
	
	transform.localRotation = Quaternion.Euler(90,180,0);
	transform.localPosition.Set(0,0,0);
}

function Update () {
	//direccion
	if (PLAYER.transform.position.x < transform.position.x && transform.localScale.x > 0){
		transform.localScale.x = -Mathf.Abs(transform.localScale.x);
//		Debug.Log("izq");
	}
	else if (PLAYER.transform.position.x > transform.position.x && transform.localScale.x < 0){
		transform.localScale.x = Mathf.Abs(transform.localScale.x);
//		Debug.Log("der");
	}
	
	if (hasAttack) {
		//check parent state para animacion de ataque
		if (PARENT.GetComponent(EnemyMove).m_State == STATE_ATTACK || PARENT.GetComponent(EnemyMove).m_State == STATE_WAITING )
			renderer.material.mainTextureOffset.y = 0;
		else
			renderer.material.mainTextureOffset.y = 0.5;
	}
	
	//loop
	if (timeCounter >= loopTime) {
		renderer.material.mainTextureOffset.x = 0;
		timeCounter = 0;
	}
	else if (timeCounter >= 0.66*loopTime) 
		renderer.material.mainTextureOffset.x = .666;
	else if (timeCounter >= 0.33*loopTime) 
		renderer.material.mainTextureOffset.x = .333;
//	else if (timeCounter >= 0.25*loopTime) 
//		renderer.material.mainTextureOffset = Vector2(0,.5);
	timeCounter += Time.deltaTime;
}