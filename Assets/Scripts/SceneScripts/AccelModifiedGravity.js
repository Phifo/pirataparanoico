#pragma strict

private var GravityFactor:float;

//para no tener una gravedad 100 lateral
var MinGravY : float = -0.05;


function Start () {
	GravityFactor = Physics.gravity.magnitude;
}

function FixedUpdate () {
//	Coordenadas para funcionar con Z como profundidad, X lateral, Y altura.
	var GravityY:float = Input.acceleration.x;
	if ( GravityY >= MinGravY )
		GravityY = MinGravY;
	
	Physics.gravity = GravityFactor * Vector3(-Input.acceleration.y, GravityY ,0).normalized;
	
}