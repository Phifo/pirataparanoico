#pragma strict

var mainMenuTexture : Texture;
var creditsTexture : Texture;

private var mainMenuPosRect : Rect;
private var scoreText : String;

// Menu State Machine
private var MAIN:byte	=	0;
private var CREDITS:byte=	1;
private var START:byte	=	2;
private var TUTORIAL:byte=	3;

private var State : byte;
private var nextState : byte;

private var rotSpeedY : float;
private var alphaValue :float;
private var alphaSpeed : float;

var TextStyle = new GUIStyle();
var loadingText = new GUIStyle();

function Start () {
	State = MAIN;
	
	mainMenuPosRect = Rect(0, Screen.height-0.5*Screen.width ,Screen.width , 0.5*Screen.width);
	
	alphaValue = 1;
	alphaSpeed = 0;
	GameObject.FindWithTag("Player").GetComponent(ParanoidAttack).enabled = false;
	scoreText = "Top Scores:\n" + PlayerPrefs.GetInt("TopScore1", 0) +"\n"  + PlayerPrefs.GetInt("TopScore2", 0) +"\n"  + PlayerPrefs.GetInt("TopScore3", 0);
}

function Update () {
	alphaValue += alphaSpeed*Time.deltaTime;
	if (alphaValue <= 0) {
		alphaSpeed = Mathf.Abs(alphaSpeed);
		State= nextState;
	}
	
	if (State == MAIN && alphaValue > 1 ) {
		transform.eulerAngles.y = 0;
		alphaValue = 1;
		alphaSpeed = 0;
	}
	else if (State == CREDITS && alphaValue > 1 ) {
		alphaValue = 1;
		alphaSpeed = 0;
	}
	else if (State == TUTORIAL && alphaValue > 1.1 ) {
		alphaValue = 1;
		alphaSpeed = 0;
		Application.LoadLevel("Tutorial");
	}
	else if (State == START && alphaValue > 1.1 ) {
		alphaValue = 1;
		alphaSpeed = 0;
		Application.LoadLevel("Level1");
	}
}

function OnGUI() {
	GUI.color = Color(GUI.color.r, GUI.color.g, GUI.color.b, alphaValue);
	//botones menu principal
	if (State == MAIN) {
		GUI.DrawTextureWithTexCoords( mainMenuPosRect, mainMenuTexture, Rect(0,0,1,1) , true);
	
		if (GUI.Button(Rect( mainMenuPosRect.x +0.07*mainMenuPosRect.width, mainMenuPosRect.y +0.1*mainMenuPosRect.height, 0.45*mainMenuPosRect.width , 0.23*mainMenuPosRect.height), "", GUI.skin.label)) {
			alphaSpeed= -1;
			nextState= START;
		}
		if (GUI.Button(Rect( mainMenuPosRect.x +0.07*mainMenuPosRect.width, mainMenuPosRect.y +0.37*mainMenuPosRect.height , 0.45*mainMenuPosRect.width , 0.23*mainMenuPosRect.height), "", GUI.skin.label)) {
			alphaSpeed= -1;
			nextState= TUTORIAL;
		}
		if (GUI.Button(Rect( mainMenuPosRect.x +0.07*mainMenuPosRect.width, mainMenuPosRect.y +0.67*mainMenuPosRect.height , 0.45*mainMenuPosRect.width , 0.23*mainMenuPosRect.height), "", GUI.skin.label)) {
			alphaSpeed= -1;
			nextState= CREDITS;
			GameObject.FindWithTag("Player").GetComponent(ParanoidAttack).enabled = false;
		}
		
		//mostrar puntaje max
		
		GUI.Label (Rect( mainMenuPosRect.x +0.6*mainMenuPosRect.width, mainMenuPosRect.y +0.22*mainMenuPosRect.height, 0.3*mainMenuPosRect.width , 0.7*mainMenuPosRect.height), scoreText, TextStyle);
	}
	
	
	//menu credits
	else if (State == CREDITS) {
		GUI.DrawTextureWithTexCoords( Rect(0,0,Screen.width,Screen.height), creditsTexture, Rect(0,0,1,1) , true);
		if (GUI.Button(Rect(0,0,Screen.width,Screen.height), "", GUI.skin.label)) {
			alphaSpeed= -1;
			nextState = MAIN;
		}
	}
	//menu tutorial
	else if (State == TUTORIAL) {
		GUI.Label (Rect(0, 0.77*Screen.height, Screen.width, 0.2*Screen.height), "Loading Tutorial", loadingText);
	}
		
	//menu gamestart
	else if (State == START) {
		GUI.Label (Rect(0, 0.77*Screen.height, Screen.width, 0.2*Screen.height), "Loading Paranoia", loadingText);
	}
}