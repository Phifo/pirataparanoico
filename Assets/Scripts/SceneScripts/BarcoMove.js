#pragma strict

var anguloMax : int = 45 ;	 	//maximo angulo de inclinacion que puede tener el barco
var rotSpeed : int = 15 ; 		//velocidad con que el barco puede rotar
private var nuevoAngulo : int ;

var precision : float = 1.0;		//que tan precisa debe ser la rotacion antes de dejar de girar

var tiempoCambio : float = 3.0 ; 	//cada cuanto se cambia el angulo
private var ultimoCambio : float;

function Start () {
	ultimoCambio = 0;
}

function Update () {
	if ( Time.time >= ultimoCambio + tiempoCambio ){
		//en vez de usar random por si solo, lo uso de esta forma para tendenciar los valores a numeros centrales
		nuevoAngulo = Random.Range(0, anguloMax) + Random.Range(0, anguloMax);
		if (Random.Range(0,2)==0)
			nuevoAngulo=-nuevoAngulo;
		ultimoCambio = Time.time;
	}
	
	//limites por anguloMax
	if ( transform.eulerAngles.z > anguloMax && transform.eulerAngles.z < 180 ){
		transform.Rotate(-precision*Vector3.forward * Time.deltaTime);
		nuevoAngulo = 0;
	}
	else if ( transform.eulerAngles.z-360 < nuevoAngulo && transform.eulerAngles.z > 180 && nuevoAngulo < 0){
		transform.Rotate(precision*Vector3.forward * Time.deltaTime);
		nuevoAngulo = 0;
	}
	
	//movimiento
	if ( nuevoAngulo > precision ) {
		transform.Rotate(rotSpeed*Vector3.forward * Time.deltaTime);
		nuevoAngulo -= rotSpeed*Time.deltaTime;
	}
	else if ( nuevoAngulo < -precision ) {
		transform.Rotate(-rotSpeed*Vector3.forward * Time.deltaTime);
		nuevoAngulo += rotSpeed*Time.deltaTime;
	}
}

function Reset() {
	//matar enemigos
	var malos = GameObject.FindGameObjectsWithTag("Enemy");
	for (var enem in malos ) {
		enem.gameObject.GetComponent(EnemyMove).DieAndDestroy();
	}

	ultimoCambio = 0;
	nuevoAngulo = 0;
	transform.localRotation = Quaternion.identity;
}