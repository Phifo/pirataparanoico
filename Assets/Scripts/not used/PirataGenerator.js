#pragma strict

var ENEMIES : GameObject;
var LAUNCH_TIME: int;
var MAX_PIRATAS : int;
var CENTRO_BARCO : float;
var ANCHO_BARCO : float;
var BARCO : GameObject;
var PLAYER : GameObject;

private var enemyCount : int = 0;
private var timer : float = 0;

function Start () {
	timer = LAUNCH_TIME;
	Random.seed = Time.time;
	enemyCount = 0;

}


function Update () {
	timer += 1;
	if (timer >= LAUNCH_TIME && enemyCount < MAX_PIRATAS)
	{
		var nEnemy : GameObject;
		nEnemy = GameObject.Instantiate(ENEMIES);
		nEnemy.GetComponent(ComportamientoPirata).BARCO = BARCO;
//		nEnemy.GetComponent(ComportamientoPirata).PLAYER = PLAYER;
		nEnemy.GetComponent(ComportamientoPirata).PLAYER =  GameObject.FindWithTag ("Player");
		
		nEnemy.transform.parent = this.transform;
		nEnemy.transform.localRotation.Set(0,0,0,0);
		nEnemy.transform.localPosition.y = 4;
		
		if(Random.value > 0.5)
			nEnemy.transform.localPosition.x = 3;
		else
			nEnemy.transform.localPosition.x = -3;

		enemyCount += 1;
		timer = 0;
	}
}