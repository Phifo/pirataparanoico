#pragma strict

function Start () {

}

function Update () {

}

//destroy all game objects that enter this area
function OnTriggerEnter  (other : Collider) {
	//matar al jugador
	if ( other.gameObject.CompareTag("Player") ) {
//		Debug.Log("al agua culiao");
		other.GetComponent(ParanoidHealth).OnDeath();
	}
	//matar al enemigo
	else if (other.gameObject.CompareTag("Enemy") )
		other.GetComponent(EnemyMove).DieAndDestroy();
	//matar cualquier otra cosa
	else
		Destroy(other.gameObject);
}