#pragma strict

private var lifetime : float;

function Start () {
//	lifetime = 1.5;
}

function Update () {
	//animacion de subir como una burbuja
	transform.position.y += 0.15*Time.deltaTime;
	lifetime += -Time.deltaTime;
	
	//cuando se le acaba el tiempo morir
	if (lifetime <= 0)
		Destroy( gameObject );
	
}

function SetText ( text:String, time:float , color:Color ) {
	guiText.text = text;
	lifetime = time;
	guiText.material.color = color;
	
}

function SetText ( text:String, time:float ) {
	guiText.text = text;
	lifetime = time;	
}