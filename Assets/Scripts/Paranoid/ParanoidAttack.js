#pragma strict

private var p0 : Vector3;
private var p1 : Vector3;

var xRESOLUTION : int = 12; //Numero de secciones horizontales
var yRESOLUTION : int = 6; //Numero de secciones verticales

private var BARCO : GameObject;
private var SPRITE : GameObject;
public var Attack : GameObject;

public var Cooldown : float = 0.5;
private var cooldownTimer : float;

//public var Menu : boolean = false;

function Start () {
	BARCO =  GameObject.FindWithTag ("Barco");
	SPRITE = GameObject.FindWithTag ("PlayerSprite");
	
	cooldownTimer = 0;
	
	p0 = Vector3(0,0,-1);
	p1 = Vector3(0,0,-1);
}


function Update () {
	if ( cooldownTimer <= 0 && Time.timeScale > 0 ) // Time.timeScale para evitar atacar en pausa
		AttackReady();
	else
		cooldownTimer += - Time.deltaTime;
}


function AttackReady() {
	for (var touch: Touch in Input.touches)
	{
		if(touch.phase == TouchPhase.Began && p0[2] == -1)
		{
			p0 = Vector3( Mathf.Floor(touch.position.x/(Screen.width/xRESOLUTION)),Mathf.Floor(touch.position.y/(Screen.height/yRESOLUTION)),touch.fingerId);
			//corregimos segun la inclinacion del barco
			p0 = Quaternion.AngleAxis(BARCO.transform.eulerAngles.z,Vector3.down) * p0;
		}		
		else if(touch.phase ==  TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
		{
		
			p1 = Vector3( Mathf.Floor(touch.position.x/(Screen.width/xRESOLUTION)),
										Mathf.Floor(touch.position.y/(Screen.height/yRESOLUTION)),touch.fingerId);
										
			//corregimos segun la inclinacion del barco
			p1 = Quaternion.AngleAxis(BARCO.transform.eulerAngles.z,Vector3.down) * p1;
		    
		    var info : String = ", debug info: pos Inicial (" + p0[0] + "," + p0[1] + "), pos Final (" + p1[0] + "," + p1[1] + ")";
		    
		    if(p1[1] - p0[1] == 0) {
		    	if(p1[0] - p0[0] >= 0 && p1[0] >= xRESOLUTION/2)
		    	{
//		    		Debug.Log("Ataque a la derecha horizontal" + info);
		    		StraightRightAttackEvent();
		    	}
		    	else if(p1[0] - p0[0] <= 0 && p1[0] < xRESOLUTION/2)
		    	{
//		    		Debug.Log("Ataque a la izquierda horizontal" + info);
		    		StraightLeftAttackEvent();
		    	}
		    }
		    else if (p1[1] - p0[1] > 0) {
		        if(p1[0] - p0[0] >= 0 && p1[0] >= xRESOLUTION/2)
		    	{
//		    		Debug.Log("Ataque a la derecha hacia arriba" + info);
		    		DownToUpRightAttackEvent();
		    	}
		    	else if(p1[0] - p0[0] <= 0 && p1[0] < xRESOLUTION/2)
		    	{
//		    		Debug.Log("Ataque a la izquierda hacia arriba" + info);
		    		DownToUpLeftAttackEvent();
		    	}
		    } 
		    else if ( p1[1] - p0[1] < 0) {
		    	if(p1[0] - p0[0] >= 0 && p1[0] >= xRESOLUTION/2)
		    	{
//		    		Debug.Log("Ataque a la derecha hacia abajo" + info);
		    		UpToDownRightAttackEvent();
		    	}
		    	else if(p1[0] - p0[0] <= 0 && p1[0] < xRESOLUTION/2)
		    	{
//		    		Debug.Log("Ataque a la izquierda hacia abajo" + info);
		    		UpToDownLeftAttackEvent();
		    	}
		    }		
			p1 = Vector3(0,0,-1); p0 = Vector3(0,0,-1);
		
		}
	
	}

}

function DownToUpRightAttackEvent () {	
	CreateAttack(1, false);}

function DownToUpLeftAttackEvent () {	
	CreateAttack(1, true);
}
function UpToDownRightAttackEvent () {	
	CreateAttack(3, false);
}
function UpToDownLeftAttackEvent () {	
	CreateAttack(3, true);
}
function StraightRightAttackEvent () {	
	CreateAttack(2, false);
}
function StraightLeftAttackEvent () {
	CreateAttack(2, true);

}

function CreateAttack (type : int, left : boolean) {
	cooldownTimer = Cooldown;

	var nAtt : GameObject;
	nAtt = GameObject.Instantiate(Attack);
	nAtt.GetComponent(AttackBullet).SetType(type);
	if (left) {
		nAtt.GetComponent(AttackBullet).SPEED = -nAtt.GetComponent(AttackBullet).SPEED;
	}
	else
		nAtt.renderer.material.mainTextureScale.x  = -1;
	//change paranoid sprite
	SPRITE.gameObject.GetComponent(ParanoidSprite).Attack(type, left ,Cooldown);
	
}
