#pragma strict

@script RequireComponent( CharacterController )

// This script must be attached to a GameObject that has a CharacterController
//var moveTouchPad : Joystick;
//var jumpTouchPad : Joystick;

var speed : float = 30;

private var thisTransform : Transform;
private var character : CharacterController;
private var velocity : Vector3 = Vector3.zero;						// Used for continuing momentum while in air
private var platformAngle : float;
private var movement : Vector3 = Vector3.zero;
private var angleY : float;

function Start()
{
	// Cache component lookup at startup instead of doing this every frame lol		
	thisTransform = GetComponent( Transform );
	character = GetComponent( CharacterController );	

	// Move the character to the correct start position in the level, if one exists
	var spawn = GameObject.Find( "PlayerSpawn" );
	if ( spawn )
		thisTransform.position = spawn.transform.position;
}

function OnEndGame()
{
	// Don't allow any more control changes when the game ends
	this.enabled = false;
}

function Update()
{
	if (platformAngle > 270)
		platformAngle = platformAngle - 360;
	
	angleY = platformAngle / 90;
	Debug.Log(Vector2(angleY, Input.acceleration.y));
	velocity.x -= (angleY + Input.acceleration.y/2) * speed;
	velocity.y -= (1 - angleY) * speed;
	
	// Actually move the character
	velocity *= Time.deltaTime;
	
	// Debug.Log(Input.acceleration.y);
	character.Move( velocity );
}

function OnControllerColliderHit(hit : ControllerColliderHit) {
    platformAngle = hit.collider.transform.eulerAngles.z;
}