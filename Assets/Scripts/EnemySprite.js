#pragma strict

private var timeCounter: float;
public  var loopTime: float = 1.5;

private var PLAYER:GameObject;

function Start () {
	timeCounter = 0;
	PLAYER =  GameObject.FindWithTag ("Player");
	
	transform.localRotation = Quaternion.Euler(90,180,0);
	transform.localPosition.Set(0,0,0);
}

function Update () {
	//direccion
	if (PLAYER.transform.position.x < transform.position.x && transform.localScale.x > 0){
		transform.localScale.x = -Mathf.Abs(transform.localScale.x);
//		Debug.Log("izq");
	}
	else if (PLAYER.transform.position.x > transform.position.x && transform.localScale.x < 0){
		transform.localScale.x = Mathf.Abs(transform.localScale.x);
//		Debug.Log("der");
	}
	
	//loop
	if (timeCounter >= loopTime) {
		renderer.material.mainTextureOffset = Vector2(.5,.5);
		timeCounter = 0;
	}
	else if (timeCounter >= 0.75*loopTime) 
		renderer.material.mainTextureOffset = Vector2(0,0);
	else if (timeCounter >= 0.5*loopTime) 
		renderer.material.mainTextureOffset = Vector2(.5,.5);
	else if (timeCounter >= 0.25*loopTime) 
		renderer.material.mainTextureOffset = Vector2(0,.5);
	timeCounter += Time.deltaTime;
}