#pragma strict

public var waterTexture : Texture2D;
public var height : float;
public var speed : float;
public var alphaMax : float;

//trozo de la pantalla donde se dibuja
private var drawArea : Rect;
//trozo de la textura que se dibuja
private var waterArea1 : Rect;
private var waterArea2 : Rect;

private var alpha1 : float;
private var alpha2 : float;

function Start () {
	drawArea = Rect(0, (1-height)*Screen.height, Screen.width, height*Screen.height);
	
	waterArea1 = Rect(0, 0, 0.25, 1);
	waterArea2 = Rect(0.375, 0, 0.25, 0.8);
}

function Update () {
	waterArea1.x += speed*Time.deltaTime;
	//posicion de water1
	if (waterArea1.x + waterArea1.width >= 1)
		waterArea1.x = 0;
	//alpha de water1
	if (waterArea1.x < 0.4)
		alpha1 = 5*waterArea1.x; // multiplicar por 5 es igual que dividir por 0.2
	else if (waterArea1.x + waterArea1.width > 0.6){
		alpha1 = 5*(1- (waterArea1.x + waterArea1.width));
	}
	else
		alpha1 = 1;
		
	
	waterArea2.x += speed*Time.deltaTime;
	//posicion de water2
	if (waterArea2.x + waterArea2.width >= 1)
		waterArea2.x = 0;
	//alpha de water2
	if (waterArea2.x < 0.4)
		alpha2 = 5*waterArea2.x; // multiplicar por 5 es igual que dividir por 0.2
	else if (waterArea2.x + waterArea2.width > 0.6){
		alpha2 = 5*(1- (waterArea2.x + waterArea2.width));
	}
	else
		alpha2 = 1;
}


function OnGUI() {
	GUI.color = Color(GUI.color.r, GUI.color.g, GUI.color.b, alphaMax*alpha1);
	GUI.DrawTextureWithTexCoords( drawArea, waterTexture, waterArea1 , true);
	
	GUI.color = Color(GUI.color.r, GUI.color.g, GUI.color.b, alphaMax*alpha2);
	GUI.DrawTextureWithTexCoords( drawArea, waterTexture, waterArea2 , true);
}