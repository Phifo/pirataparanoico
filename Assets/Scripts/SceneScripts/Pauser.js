#pragma strict

var PAUSE : boolean = false;

private var PLAYER: GameObject;
var TextStyle = new GUIStyle();
var attackType : boolean = false;

function Start () {
	if (PAUSE)
		Pause();
	PLAYER =  GameObject.FindWithTag ("Player");
}

function Update () {

	if(Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape)) {
		if(!PAUSE)
			Pause();
		else
			Resume();
	}
}

function Resume () {
	AudioListener.pause = false;
	Time.timeScale = 1.0;
	PAUSE = false;
}

function Pause () {
	AudioListener.pause = true;
	Time.timeScale = 0.0;
	PAUSE = true;
}


function OnGUI () {
	if (PAUSE) {
		if ( GUI.Button(Rect( 0.05*Screen.width, 0.2*Screen.height, 0.4*Screen.width , 0.2*Screen.height), "Resume", TextStyle) )
			Resume();
		if ( GUI.Button(Rect( 0.55*Screen.width, 0.2*Screen.height, 0.4*Screen.width , 0.2*Screen.height), "Main Menu", TextStyle) ) {
			Application.LoadLevel ("StartMenu");
			Resume();
		}
		if ( GUI.Button(Rect( 0.1*Screen.width, 0.45*Screen.height, 0.8*Screen.width , 0.2*Screen.height), "Button Style Attack:"+attackType, TextStyle) )
			changeAttackMode();
		if ( GUI.Button(Rect( 0.3*Screen.width, 0.7*Screen.height, 0.4*Screen.width , 0.2*Screen.height), "Quit Game", TextStyle) )
			Application.Quit();
	}
}

function changeAttackMode() {
	if ( PLAYER.GetComponent(ParanoidAttack).enabled ) {
		PLAYER.GetComponent(ParanoidAttack).enabled = false;
		PLAYER.GetComponent(ParanoidAttack2).enabled = true; 
		attackType = true;
	}
	else {
		PLAYER.GetComponent(ParanoidAttack).enabled = true;
		PLAYER.GetComponent(ParanoidAttack2).enabled = false; 
		attackType = false;
	}
	Debug.Log("yupi"+attackType);
}