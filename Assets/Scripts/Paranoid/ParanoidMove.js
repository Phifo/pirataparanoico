#pragma strict
private var BARCO : GameObject; 

public var maxSpeed : float = 4;
public var minSpeed : float = 0.05;

private var speed : float ;

//Paranoid State Machine
private var STATE_MOVE:byte		=	0;
private var STATE_FALLING:byte		=	1;

private var m_State  :  byte;

function Start () {
	//parent barco
	BARCO =  GameObject.FindWithTag ("Barco");
	transform.parent = BARCO.transform;
	
	transform.localRotation = Quaternion.identity;
	transform.localPosition = Vector3(0, transform.collider.bounds.extents.y, 0);
	
	speed = 0;
	
	m_State = STATE_MOVE;
}

function Update () {
	switch (m_State) {
	
		case STATE_MOVE:
			s_Move();
			break;
		
		case STATE_FALLING:
			s_Falling();
			break;
		
		default:
			Debug.Log("ERROR: Estado no válido en enemigo. EnemyMove.js");
		break;
	}
}

function s_Move() {
	speed = Vector3.Angle( -transform.up, Physics.gravity );
	speed = maxSpeed* speed/90 ;
	if ( Vector3.Angle( transform.right, Physics.gravity ) > 90 )
		speed = -speed;
	
	//si la velocidad es muy poca, no mover %*maxspeed
	if ( Mathf.Abs(speed) > minSpeed)
		transform.localPosition.x += speed *Time.deltaTime;
	
	//se cae o no se cae
	if ( Mathf.Abs(transform.position.x) - BARCO.transform.collider.bounds.extents.x > 0 ) {
		m_State= STATE_FALLING;
		speed = 3;
	}
}

function s_Falling() {
	speed += Physics.gravity.magnitude * Time.deltaTime; //aceleracion
	transform.position.y += -speed * Time.deltaTime; //desplazamiento
	transform.Rotate(0,0, 360*Time.deltaTime); //rotacion, just for fun
}


//arroja al pirata muerto por la borda
function Suicide () {
	if (m_State != STATE_FALLING) {
		m_State= STATE_FALLING;
		speed = -3;
	}
}