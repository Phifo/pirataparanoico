#pragma strict

//velocidad maxima
var speed : float = 2.0;

private var moveDirection = Vector3.zero;
private var Yspeed : float = 0.0;

private var normal = Vector3.zero;

function Start () {

}


function FixedUpdate () {
    var controller : CharacterController = GetComponent(CharacterController);
	
	//si esta en el piso
	if (controller.isGrounded) {
		//calcular la componente de la gravedad que es paralela a la normal de colision con el piso
		//se usa la gravedad normalizada para que su magnitud no afecte directamente la velocidad
		var projection = Vector3.Project(Physics.gravity.normalized, normal);
		//si a la gravedad le restamos esa componente, nos queda solo la que es perpendicular al piso
		//que es la direccion en que nos queremos mover
		moveDirection = (Physics.gravity.normalized - projection);
		//moveDirection no es un vector normalizado, ya que fue creado con solo una parte de uno
		//si normalizado, por lo tanto su modulo, es como maximo 1, y como minimo 0
		
		//asi al multiplicar por la velocidad, tenemos un rango de movimiento entre 0 y la velocidad del pj
		controller.Move( speed* moveDirection  * Time.deltaTime);
	}
	else {
		//si no esta en el piso, no hay normal, su movimiento es segun la gravedad
		moveDirection += Physics.gravity *Time.deltaTime;
		
		controller.Move( moveDirection  * Time.deltaTime);
	}

}

@script RequireComponent(CharacterController)

//la normal la obtenemos con la funcion que detecta colisiones, y pidiendole la normal a la colision
function OnControllerColliderHit (hit : ControllerColliderHit) {
//    if (hit.collider.attachedRigidbody.isKinematic)
        normal = hit.normal;
}


function OnDeath () {
	Application.Quit();
}