#pragma strict

@script RequireComponent(AudioSource)
public var attackAudio1: AudioClip;
public var attackAudio2: AudioClip;

//Paranoid State Machine
private var IDLE:byte	=	0;
private var ATTACK:byte	=	1;
private var ATT_LD:byte	=	1;

private var STATE;

private var actionTime : float = 0.5;
private var actionTimeCounter : float = 0;

var offsetY : float = 0.145;

function Start () {
	ToIdle();
}

function Update () {
	switch (STATE) {
	
		case IDLE:
			actionTimeCounter += 0.7*Time.deltaTime;
			if ( actionTimeCounter >= actionTime ) {
				actionTimeCounter = 0;
			}
			else if ( actionTimeCounter >= 0.6*actionTime )
				renderer.material.mainTextureOffset.x = 0.666;
			else if ( actionTimeCounter >= 0.3*actionTime )
				renderer.material.mainTextureOffset.x = 0.333;
			else
				renderer.material.mainTextureOffset.x = 0.0;
			break;
		
		case ATTACK:
			actionTimeCounter += Time.deltaTime;
			if ( actionTimeCounter >= actionTime+0.2 ) {
				ToIdle();
				actionTimeCounter = 0;
			}
			else if ( actionTimeCounter >= 0.6*actionTime )
				renderer.material.mainTextureOffset.x = 0.666;
			else if ( actionTimeCounter >= 0.333*actionTime )
				renderer.material.mainTextureOffset.x = 0.333;
			break;
		
		default:
			Debug.Log("ERROR: Estado no válido en PirataSprite");
		break;
	}
}

function Attack(type : int, left : boolean, timer : float) {
	switch (type) {
		//upToDown
		case 3:
			if (left)
				renderer.material.mainTextureOffset.y = 4*offsetY;
			else
				renderer.material.mainTextureOffset.y = 5*offsetY;
			audio.PlayOneShot(attackAudio1);
			break;
		//straightSide
		case 2:
			if (left)
				renderer.material.mainTextureOffset.y = 0;
			else
				renderer.material.mainTextureOffset.y = offsetY;
			audio.PlayOneShot(attackAudio2);
			break;
		//downToUp
		case 1:
			if (left)
				renderer.material.mainTextureOffset.y = 2*offsetY;
			else
				renderer.material.mainTextureOffset.y = 3*offsetY;
			audio.PlayOneShot(attackAudio2);
			break;
		default:
			renderer.material.mainTextureOffset.y = 6*offsetY;
			break;
	}
	STATE = ATTACK;
			
	actionTime = timer;
}

function ToIdle(){
	STATE = IDLE;
	renderer.material.mainTextureOffset.y = 6*offsetY;
	actionTime = 0.4;
}