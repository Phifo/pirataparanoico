#pragma strict

private var BARCO : GameObject;
private var SPRITE : GameObject;
public var Attack : GameObject;

public var Cooldown : float = 0.5;
private var cooldownTimer : float;
var canAttack : boolean;
//public var Menu : boolean = false;


private var TextStyle = new GUIStyle();

function Start () {
	BARCO =  GameObject.FindWithTag ("Barco");
	SPRITE = GameObject.FindWithTag ("PlayerSprite");
	
	cooldownTimer = 0;
	canAttack = true;
}


function Update () {
	if ( cooldownTimer <= 0 && Time.timeScale > 0 ) // Time.timeScale para evitar atacar en pausa
//		AttackReady();
		canAttack = true;
	else {
		cooldownTimer += - Time.deltaTime;
		canAttack = false;
	}
}


function AttackReady() {
	
}

function OnGUI () {
	if ( canAttack ) {
//		Debug.Log("holi");
		if ( GUI.Button(Rect( 0, 0, 0.5*Screen.width , 0.333*Screen.height), "", TextStyle) )
			CreateAttack(1, true);
		else if ( GUI.Button(Rect( 0.5*Screen.width, 0, 0.5*Screen.width , 0.333*Screen.height), "", TextStyle) )
			CreateAttack(1, false);
		else if ( GUI.Button(Rect( 0, 0.333*Screen.height, 0.5*Screen.width , 0.333*Screen.height), "", TextStyle) )
			CreateAttack(2, true);
		else if ( GUI.Button(Rect( 0.5*Screen.width, 0.333*Screen.height, 0.5*Screen.width , 0.333*Screen.height), "", TextStyle) )
			CreateAttack(2, false);
		else if ( GUI.Button(Rect( 0, 0.666*Screen.height, 0.5*Screen.width , 0.333*Screen.height), "", TextStyle) )
			CreateAttack(3, true);
		else if ( GUI.Button(Rect( 0.5*Screen.width, 0.666*Screen.height, 0.5*Screen.width , 0.333*Screen.height), "", TextStyle) )
			CreateAttack(3, false);
		
	}
}



function CreateAttack (type : int, left : boolean) {
	cooldownTimer = Cooldown;

	var nAtt : GameObject;
	nAtt = GameObject.Instantiate(Attack);
	nAtt.GetComponent(AttackBullet).SetType(type);
	if (left) {
		nAtt.GetComponent(AttackBullet).SPEED = -nAtt.GetComponent(AttackBullet).SPEED;
	}
	else
		nAtt.renderer.material.mainTextureScale.x  = -1;
	//change paranoid sprite
	SPRITE.gameObject.GetComponent(ParanoidSprite).Attack(type, left ,Cooldown);
	
}
