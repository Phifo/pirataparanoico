#pragma strict

public var action : int ;
//public var nextLevel : String;

var tutorialHandler : GameObject;
var tutorial : boolean;

function Start () {
	action = 0;
}

function Update () {

}


function hit(type : int) {
	if (tutorial && action != 3) {
		if (type == 3 && action == 0) {
			tutorialHandler.GetComponent(Tutorial).PHASE += 1;
			action = 1;
		}
		else if (type == 1 && action == 1) {
			tutorialHandler.GetComponent(Tutorial).PHASE += 1;
			action = 2;
		}
		else if (type == 2  && action == 2) {
			tutorialHandler.GetComponent(Tutorial).PHASE += 1;
			action = 3;
		}
	}
	else if (tutorial && action == 3) {
		tutorialHandler.GetComponent(Tutorial).PHASE += 1;
		GameObject.FindWithTag("Player").GetComponent(ParanoidHealth).enabled = true;
		GameObject.FindWithTag("Player").GetComponent(ParanoidHealth).vidas = 999;
		GameObject.FindWithTag("Player").GetComponent(ParanoidMove).enabled = true;
		GameObject.FindWithTag("Barco").GetComponent(BarcoMove).enabled = true;
		Destroy( gameObject );
	}
	
}