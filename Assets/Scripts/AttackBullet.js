#pragma strict

public var SPEED : float = 2;
// tipo de ataque, 1 = ascendente, 2 horizontal, 3 descendente
public var TYPE : int = 1;
//distancia a la que muere el ataque
public var DISTANCE : float = 2;
private var travelledDist : float = 0;

public var popUp : GameObject;

//texturas para cada tipo
public var textures : Material[];

function Start () {
	//set parent
	transform.parent = GameObject.FindWithTag("Barco").transform;
	//set position
	transform.localPosition = GameObject.FindWithTag("Player").transform.localPosition;
}

function Update () {
	transform.localPosition.x += SPEED * Time.deltaTime;
	travelledDist += SPEED * Time.deltaTime;
	
	if (Mathf.Abs( travelledDist ) > DISTANCE) {
		//crear el MISS popup y destruirse
		var pos = Vector3( Mathf.Sign(SPEED)*0.2+0.44, 0.6, 0 );
		var obj = Instantiate (popUp, pos, Quaternion.identity );
		obj.GetComponent(popText).SetText("MISS",.5,Color.red );
		Destroy( gameObject );
	}
}

//function OnCollisionEnter(collision : Collision) {
function OnTriggerEnter (collision : Collider) {
//	Debug.Log( collision.gameObject.name );
	if ( collision.gameObject.CompareTag("Enemy") ) {
		collision.gameObject.GetComponent(EnemyMove).hit(TYPE);
		
		Destroy( gameObject ); //autodestruirse
	}
	//esto es para los menu
	else if ( collision.gameObject.CompareTag("MenuElement") ) {
		collision.gameObject.GetComponent(MenuElement).hit(TYPE);
		Destroy( gameObject ); //autodestruirse
	}
	
}

function SetType ( type : int ) {
	TYPE = type;
	renderer.material = textures[TYPE-1];
}