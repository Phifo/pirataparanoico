#pragma strict

var ENEMIES : GameObject[]; //lista de enemigos que pueden aparecer
var MAX_ENEMIES : int = 6; //max cantidad de enemigos activos
var LAUNCH_TIME: float = 2;
var varEnemies : int = 1; //numero de variaciones de enemigos permitidas

private var enemyCount : int = 0; //conteo de enemigos activos
private var timer : float = 0;

function Start () {
	
}

function Update () {
	timer += Time.deltaTime;
	if (timer > LAUNCH_TIME && enemyCount < MAX_ENEMIES) {
		
		//elegir cual enemigo crear
		var select = Random.Range(0,varEnemies);
		//crearlo
		var nEnemy : GameObject;
		nEnemy = GameObject.Instantiate(ENEMIES[select]);
		
		enemyCount += 1;
		timer = 0;
	}
}

function EnemyKilled() {
	enemyCount += -1;
}