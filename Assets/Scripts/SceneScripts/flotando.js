#pragma strict

public var LimitX : float;
public var LimitY : float;

public var speedX : float;
public var speedY : float;

private var initX : float;
private var initY : float;



function Start () {
	initX = transform.localPosition.x;
	initY = transform.localPosition.y;
}

function Update () {
	if ( Mathf.Abs(transform.localPosition.x - initX) > LimitX )
		speedX = -speedX;
	if ( Mathf.Abs(transform.localPosition.y - initY) > LimitY )
		speedY = -speedY;
	
	if ( speedX != 0 )
		transform.localPosition.x += speedX*Time.deltaTime;
	if ( speedY != 0 )
		transform.localPosition.y += speedY*Time.deltaTime;
}