#pragma strict

@script RequireComponent(AudioSource)
public var hurtAudio1: AudioClip;
public var hurtAudio2: AudioClip;

private var healthLenght : int = Screen.width;
private var myRect : Rect = Rect(0,0,1,1);
private var fAlpha : float = 1.0;
public var minHealthAlpha : float = 0.25;

public var vidas : int = 1;


//Textura del GUI
public var blood : Texture;
public var health : int = 100;

//settings de texto
var TextStyle = new GUIStyle();


function Start () {
	healthLenght = Screen.width;
	
}

function Update () {
	if ( fAlpha > minHealthAlpha ) {
		fAlpha += - 0.15 * Time.deltaTime;
	}
	myRect = Rect(0,0,1f*healthLenght/Screen.width,1);
	
	if (health <= 0) {
		gameObject.GetComponent(ParanoidMove).Suicide();
//		OnDeath(); //cambiar esto
	}
}

function OnGUI() {
	GUI.color = Color(GUI.color.r, GUI.color.g, GUI.color.b, fAlpha);
	GUI.DrawTextureWithTexCoords( Rect(0, 0.13*Screen.height ,healthLenght , -0.13*Screen.height),blood, myRect , true);
	
	GUI.color = Color(GUI.color.r, GUI.color.g, GUI.color.b, 1);
	GUI.Label (Rect (0.05*Screen.width, 0.05*Screen.height, 0.35*Screen.width, 0.1*Screen.height), "Lifes: "+vidas, TextStyle);
}

//dif es el porcentaje de vida que aumenta o disminuye
function AddHealth ( dif :int  ) {
	health += dif;
	if (health > 100)
		health = 100;
	
	healthLenght = health*Screen.width/100;
	
	if (dif < 0)
		fAlpha = 1.0;
	else if (dif > 3 && health != 100)
		fAlpha *= 1.5;
	
	//play sound when losing health
	if (dif < 0 && !audio.isPlaying) {
		if (Random.Range(0,2))
			audio.PlayOneShot(hurtAudio1);
		else
			audio.PlayOneShot(hurtAudio2);
	}
}

function OnDeath() {
	vidas += -1;
	
	//mientras no se acaben las vidas
	if (vidas >0) {
		//mientras no se acaben las vidas
		health = 100;
		
		//reset barco
		GameObject.FindWithTag ("Barco").GetComponent(BarcoMove).Reset();
		
		//reset pirata paranoico
		GetComponent(ParanoidMove).Start();
		
		healthLenght = Screen.width;
	}
	
	//Se le acabaron las vidas
	else {
		//guardar puntaje
		var topScore1 = PlayerPrefs.GetInt("TopScore1", 0);
		var topScore2 = PlayerPrefs.GetInt("TopScore2", 0);
		var topScore3 = PlayerPrefs.GetInt("TopScore3", 0);
		var currentScore = GameObject.FindWithTag("scoreText").GetComponent(ScoreText).SCORE;
		if ( currentScore > topScore1 ) {
			PlayerPrefs.SetInt("TopScore3", topScore2);
			PlayerPrefs.SetInt("TopScore2", topScore1);
			PlayerPrefs.SetInt("TopScore1", currentScore);
		}
		else if ( currentScore > topScore2 ) {
			PlayerPrefs.SetInt("TopScore3", topScore2);
			PlayerPrefs.SetInt("TopScore2", currentScore);
		}
		else if ( currentScore > topScore3 ) {
			PlayerPrefs.SetInt("TopScore3", currentScore);
		}
		Application.LoadLevel ("StartMenu");
	}
	
}