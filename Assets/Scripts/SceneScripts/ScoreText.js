#pragma strict

public var SCORE: int;
private var TIER: int;
private var BARCO: GameObject;
private var EnemGenerator: EnemyGenerator;

var TextStyle = new GUIStyle();


function Start () {
	SCORE = 0;
	TIER = 0;

	BARCO = GameObject.FindWithTag("Barco");
	EnemGenerator = BARCO.GetComponent(EnemyGenerator);
}

function Update () {
	if ( SCORE >= 7  && TIER == 0) {
		EnemGenerator.varEnemies = 2;
		TIER += 1;
	}
	else if ( SCORE >= 18  && TIER == 1) {
		EnemGenerator.varEnemies = 3;
		TIER += 1;
	}
	else if ( SCORE >= 30  && TIER == 2) {
		EnemGenerator.varEnemies = 4;
		EnemGenerator.MAX_ENEMIES += 1;
		TIER += 1;
	}
	else if ( SCORE >= 50  && TIER == 3) {
		EnemGenerator.varEnemies = 5;
		EnemGenerator.LAUNCH_TIME *= 0.7;
		EnemGenerator.MAX_ENEMIES += 1;
		TIER += 1;
	}
	else if ( SCORE >= 65  && TIER == 4) {
		EnemGenerator.LAUNCH_TIME *= 0.7;
		EnemGenerator.MAX_ENEMIES += 2;
		TIER += 1;
	}
	else if ( SCORE >= 80  && TIER == 5) {
		EnemGenerator.LAUNCH_TIME *= 0.7;
		EnemGenerator.MAX_ENEMIES += 2;
		TIER += 1;
	}
	else if ( SCORE >= 100  && TIER == 6) {
		EnemGenerator.LAUNCH_TIME *= 0.85;
		EnemGenerator.MAX_ENEMIES += 2;
		TIER += 1;
	}
	
}

function AddScore(points: int) {
	SCORE += points;
//	guiText.text = "Score: "+SCORE;
}

function OnGUI() {
	GUI.Label (Rect (0.5*Screen.width, 0.05*Screen.height, 0.45*Screen.width, 0.1*Screen.height), "Score: "+SCORE, TextStyle);
}