#pragma strict

var ButtonStyle = new GUIStyle();
var TextStyle = new GUIStyle();
var PHASE : int = 0;

function Start () {
	PHASE = 0;
}

function Update () {

}

function OnGUI () {
	if ( GUI.Button(Rect( 0, 0, 0.3*Screen.width , 0.12*Screen.height), "Back to Menu", ButtonStyle) )
		Application.LoadLevel ("StartMenu");
		
	switch (PHASE){
	case 0:
		GUI.Label (Rect(0.325*Screen.width, 0.02*Screen.height , 0.625*Screen.width, 0.12*Screen.height), "Attacking", TextStyle);
		GUI.Label (Rect(0.05*Screen.width, 0.13*Screen.height, 0.9*Screen.width, 0.15*Screen.height), "Swipe down on the right side of the screen", TextStyle);
	break;
	case 1:
		GUI.Label (Rect(0.325*Screen.width, 0.02*Screen.height , 0.625*Screen.width, 0.12*Screen.height), "Attacking", TextStyle);
		GUI.Label (Rect(0.05*Screen.width, 0.13*Screen.height, 0.9*Screen.width, 0.15*Screen.height), "Now Swipe up", TextStyle);
	break;
	case 2:
		GUI.Label (Rect(0.325*Screen.width, 0.02*Screen.height , 0.625*Screen.width, 0.12*Screen.height), "Attacking", TextStyle);
		GUI.Label (Rect(0.05*Screen.width, 0.13*Screen.height, 0.9*Screen.width, 0.15*Screen.height), "Tap or Swipe straight to the side", TextStyle);
	break;
	case 3:
		GUI.Label (Rect(0.325*Screen.width, 0.02*Screen.height , 0.625*Screen.width, 0.12*Screen.height), "Use attacks to kill enemies.", TextStyle);
		GUI.Label (Rect(0.05*Screen.width, 0.13*Screen.height, 0.9*Screen.width, 0.15*Screen.height), "Beware that enemies may block some attacks", TextStyle);
	break;
	case 4:
		GUI.Label (Rect(0.325*Screen.width, 0.02*Screen.height , 0.625*Screen.width, 0.12*Screen.height), "Stay on the boat", TextStyle);
		GUI.Label (Rect(0.05*Screen.width, 0.13*Screen.height, 0.9*Screen.width, 0.15*Screen.height), "Tilt your phone to avoid falling", TextStyle);
		GUI.Label (Rect(0.05*Screen.width, 0.29*Screen.height, 0.9*Screen.width, 0.15*Screen.height), "That's all pirate, now go back and fight", TextStyle);
	break;
	}	
	
}