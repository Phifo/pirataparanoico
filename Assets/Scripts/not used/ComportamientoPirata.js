#pragma strict

//Estados comportamiento Pirata
//IDLE: Estado default, hace nada
//APPROACHING: El pirata se acerca a atacar al jugador, animado
//ATTACK: El pirata ejecuta el ataque, animado
//HURT: El pirata recibe daño, probablmente sería empujado hacia atrás, animado
//WAITING: Dada cierta inclinación en el barco el pirata se coloca en posición de ataque quieto
//DYING: El pirata ha muerto por lo que ocurre una animación de derrota.
private var STATE_IDLE:byte 		=	0;
private var STATE_APPROACHING:byte 	=	1;
private var	STATE_ATTACK:byte		=	2;
private var STATE_HURT:byte			=	3;
private var STATE_WAITING:byte		=	4;
private var STATE_DYING	:byte		=	5; 

//Objeto que representa el barco (piso)
public var BARCO:GameObject;
//Objeto que representa el jugador
public var PLAYER:GameObject; 

//Vida inicial
public 	var INIT_HEALTH = 100;
public  var maxSpeed = 10;

private var m_Health :  float;
private var m_State  :  byte;
private var m_Speed  :  float;



function Start () {

	m_Health = INIT_HEALTH;
	m_State = STATE_IDLE;
	this.rigidbody.isKinematic = false;
	
}

function Update () {

	if(m_Health <= 0)
		m_State = STATE_DYING;

	switch(m_State)
	{
	case STATE_IDLE:
		break;
	case STATE_APPROACHING:
		//TODO:
		if(this.transform.position.x - PLAYER.transform.position.x > 0) //jugador a la izquierda
		{
			if(BARCO.transform.eulerAngles.z > 270)
				m_Speed =  -1 * maxSpeed * (1 - (BARCO.transform.eulerAngles.z%90)/90);
			else
				m_Speed =  -1 * maxSpeed * (1 + (BARCO.transform.eulerAngles.z%90)/90);
			
		} else {
		
			if(BARCO.transform.eulerAngles.z > 270)
				m_Speed =  maxSpeed * (1 + (BARCO.transform.eulerAngles.z%90)/90);
			else 
				m_Speed =  maxSpeed * (1 + (BARCO.transform.eulerAngles.z%90)/90);
		}
		this.transform.localPosition.x += m_Speed *Time.deltaTime;
		break;
	case STATE_ATTACK:
		//TODO:
		break;
	case STATE_HURT:
		//TODO:
		break;
	case STATE_WAITING:
		//TODO:
		break;
	case STATE_DYING:
		//TODO:
		break;
	default:
		Debug.Log("ERROR: Estado no válido en pirata. ComportamientoPirata.js");
		break;
	}
	
}

function OnCollisionEnter(choque : Collision) 
{
	if(m_State == STATE_IDLE && choque.gameObject == BARCO)
	{	
		m_State = STATE_APPROACHING;
		this.transform.rotation = BARCO.transform.rotation;
		this.rigidbody.isKinematic = true;
	}

}

function OnCollisionLeave(choque : Collision)
{

	if(m_State != STATE_IDLE && choque.gameObject == BARCO)
	{
		this.rigidbody.isKinematic = false;
	
	}
}